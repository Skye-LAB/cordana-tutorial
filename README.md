# Cordana Tutorial

## Table Of Content
- [Dependency](#dependency)
    1. [NodeJS](#node)
    1. [OpenJDK](#jdk)
    1. [Gradle](#gradle)
    1. [Android SDK](#sdk)
    1. [Cara Mudah Menunduh JDK dan Gradle](#mudah)
- [Cordova Installation](#install)

<a name="dependency"></a>

## Dependency

<a name="node"></a>

### 1. NodeJS 

</br>

### NodeJS for Linux

**Arch Linux**

~~~bash
   sudo pacman -S nodejs-lts-fermium npm
~~~

**Debian and Ubuntu Based Linux**

~~~bash
   sudo apt install nodejs npm
~~~

### NodeJS for Windows

Unduh installer nodejs [disini][nodejs]! Pilih 32-bit atau 64-bit tergantung dengan laptop/pc Anda. Lalu jalankan installer yang telah diunduh.

</br>

<a name="jdk"></a>

### 2. OpenJDK

Kita akan memakai OpenJDK versi ke-8

</br>

### OpenJDK for Linux

**Arch Linux**

```bash
   sudo pacman -S jdk8-openjdk
```

**Debian and Ubuntu Based Linux**

~~~bash
   sudo add-apt-repository ppa:openjdk-r/ppa
   sudo apt update 

   sudo apt install openjdk-8-jdk
~~~

> Untuk memastikan apakah sudah terpasang, jalankan command `java -v`

### OpenJDK for Windows

Buka laman [ini][jdk]! Pilih 32-bit atau 64-bit tergantung dengan laptop/pc Anda. Login/register lalu file akan diunduh dengan segera jika sudah selesai, jalankan file tersebut!

</br>

<a name="gradle"></a>

### 3. Gradle

</br>

### Gradle for Linux

**Arch Linux**

```bash
   sudo pacman -S gradle
```

**Debian and Ubuntu Based Linux**

```bash
   wget https://services.gradle.org/distributions/gradle-7.1.1-bin.zip -P /tmp

   sudo unzip -d /opt/gradle /tmp/gradle-*.zip
```

Buat file baru bernama `gradle.sh` di folder `/etc/profile.d/`

```bash
   sudo vi /etc/profile.d/gradle.sh
```

Isikan `code` berikut:

```bash
   export GRADLE_HOME=/opt/gradle/gradle-7.1.1
   export PATH=${GRADLE_HOME}/bin:${PATH}
```

> Untuk keluar vi, tekan `ecs` lalu `:q`

Lalu jalankan command berikut:

```bash
   source /etc/profile.d/gradle.sh
```

> Untuk memastikan apakah sudah terpasang, jalankan command `gradle -v`

### Gradle for Windows

Unduh berkas distribusi Gradle pada tautan [ini][gradle]. Anda bisa memilih binary-only atau complete yang disertai dengan dokumentasi. Kemudian extract file tersebut.

<a name="var"></a>

Kemudian lalukan langkah-langkah berikut:
1. Buka Control Panel kemudian masukkan system variable pada kolom pencarian.
1. Pilih Edit the system environment variables pada hasil pencarian.
1. Pada jendela yang tampil, klik tombol Environment Variables yang berada paling bawah pada jendela yang tampil.
1. Pilih variable **Path** kemudian klik **Edit**
1. Setelah jendela terbuka, klik tombol **New** dan _paste_-kan lokasi dimana Anda meng-extract file tadi.

> Untuk memastikan apakah sudah terpasang, buka **Command Prompt** jalankan command `gradle -v`

<a name="sdk"></a>

### 4. Andorid SDK

Untuk cara mudahnya Anda bisa menunduh `Android Studio`

### Andorid Studio for Linux

**Arch Linux**

Anda dapat menunduh yay [disini][yay]

```bash
   yay -S android-studio
```

**Debian and Ubuntu Based Linux**

```bash
   sudo add-apt-repository ppa:maarten-fonville/android-studio
   sudo apt update
   sudo apt install android-studio
```

### Andorid Studio for Windows
Anda dapat mengunduh Android Studio [disini][AS]

### Menginstall Android SDK melalui Andoroid Studio

1. Lauch Android Studio,
1. Selesaikan configurasinya,
1. Klik `configure` dan pilih `SDK Manager`, lalu pilih yang ingin diinstall, klik `Apply` dan tunggu sampai selesai.
1. Atau bisa melalui Project Sructure -> System Settings -> Android SDK.

</br>

Jika pc/laptop Anda tidak memumpuni untuk menjalankan Android Studio, Anda bisa mengikuti langkah-langkah berikut:

### Android SDK Command Line Tools for Arch Linux

Anda dapat menunduh yay [disini][yay]

```bash
   yay -S android-sdk-cmdline-tools-latest
```

Tambahkan `code` ini di `.bash_profile` Anda.

```bash
   export ANDROID_HOME=$HOME/Android/Sdk
   export PATH=$ANDROID_HOME/tools/bin/:$PATH
   export PATH=$ANDROID_HOME/emulator/:$PATH
   export PATH=$ANDROID_HOME/platform-tools/:$PATH
```

### Android SDK Command Line Tools for Windows and Debian/Ubuntu Base Linux

1. Unduh SDK Command Line Tools [disini][cmdTool]
2. Buat folder `android` dan extract file tersebut disana.

```
   android
      └── cmdline-tools
         └── tools
            ├── NOTICE.txt
            ├── bin
            ├── lib
            └── source.properties
```

**For Debian/Ubuntu Based Linux**

Tambahkan `code` ini di `.bash_profile` Anda.

```bash
   export ANDROID_HOME=$HOME/android
   export PATH=$ANDROID_HOME/cmdline-tools/tools/bin/:$PATH
   export PATH=$ANDROID_HOME/emulator/:$PATH
   export PATH=$ANDROID_HOME/platform-tools/:$PATH
```

**For Windows**

Buka `system environment variables` seperti cara [disini](#var).

1. Klik **New** pada kolom **System variable**,
1. Isikan `ANDROID_HOME` untuk nama dan lokasi folder android tadi untuk valuenya lalu klik **Ok**,
1. Pilih variable **Path** kemudian klik **Edit**
1. Setelah jendela terbuka, klik tombol **New** dan isikan lokasi binari dari command line tools tadi `android -> cmdline-tools -> bin` atau `C:\Users\name\android\cmdline-tools\tools\bin\`

> Untuk memastikan apakah untuk terpasang, jalankan command `sdkmanager`

<a name="mudah"></a>

### 5. Cara Mudah Mengunduh OpenJDK dan Gradle

Cara ini hanya untuk **Linux OS**

**Install Curl**

Arch Linux

```bash
   sudo pacman -S curl
```

Debian/Ubuntu Based Linux

```bash
   sudo apt install curl
```

Lalu jalankan command berikut:

```bash
   curl -s "https://get.sdkman.io" | bash # instal sdkman

   source "$HOME/.sdkman/bin/sdkman-init.sh"
```

Jalankan command berikut untuk mamastikan SDKMAN sudah terinstall.

```bash
   sdk version
```

**Menginstall Java dengan SDKMAN**

```bash
   sdk install java 8.0.302-open
```

Untuk melihat daftar Java, jalankan command berikut:

```bash
   sdk list java
```

Simbol `>>>` menunjukkan versi Java mana yang dipakai, untuk mengganti versi java yang digunakan dapat menjalankan command:

```bash
   sdk default java <isi dengan versi Java yang Anda inginkan>
```

**Menginstall Gradle dengan SDKMAN**

```bash
   sdk install gradle 7.1.1
```

> Untuk memastikan Gradle sudah terpasang, jalankan command `gradle -v`

<a name="install"></a>

## Cordova Installation

```bash
   sudo npm i -g cordova
```

Jika di Windows, jalankan **Command Prompt** _As Administrator_ dan jalankan command diatas tanpa **sudo**.

### Create a Project Cordova

Membuat proyek Cordova dengan menjalankan command:

```shell
   cordova create hello com.example.hello HelloWorld

   cd HelloWorld
```

### Adding More Platform

Untuk melihat platform apa saja yang tersedia, bisa menjalankan command berikut:

```bash
   cordova platform list
```

Menginstall platform Android dengan cara:

```bash
   cordova platform add android
```

Atau platform Browser;

```bash
   cordova platform add browser
```

### Running Cordova Project

Untuk menjalankan di Browser:

```bash
   cordova run browser
```

Untuk menjalankan di Android:

```bash
   cordova build android
```

Untuk menjadikan file `.apk`-nya

Jika sudah selesai file `.apk` akan berada di `HelloWorld/platforms/android/app/build/outputs/apk/debug/app-debug.apk`

Jika aplikasi dijalankan maka akan muncul seperti ini:

[<img src="./utils/screenshot/app-debug-small.jpeg"/>](./utils/screenshot/app-debug.jpeg)



[nodejs]: https://nodejs.org/en/download/
[jdk]: https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
[gradle]: https://gradle.org/releases/
[AS]: https://developer.android.com/studio
[cmdTool]: https://developer.android.com/studio#command-tools
[yay]: https://github.com/Jguer/yay